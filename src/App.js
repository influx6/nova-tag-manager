import React, { Component } from 'react';
import TagList from './components/TagList';
import AppHeader from './components/AppHeader';
import LoginForm from './components/LoginForm';
import AuthContext from './context/AuthContext';
import { logout, getUserDisplayName } from './utils/auth';

class App extends Component {
  constructor() {
    super();

    const initialAuthData = {
      userId: localStorage.getItem('mx_user_id'),
      accessToken: localStorage.getItem('mx_access_token'),
      hsUrl: localStorage.getItem('mx_homeserver_url'),
      displayName: localStorage.getItem('nv_display_name'),
    };

    const isLoggedIn = Object.keys(initialAuthData).reduce((acc, key) => {
      if (!acc) return false;
      return Boolean(initialAuthData[key]);
    }, true);

    this.state = {
      authData: initialAuthData,
      isLoggedIn,
      isFakeLogin: true,
      onLogin: this.onLogin,
      onLogout: this.handleLogout,
      fakeLoading: false,
    };
  }

  componentDidMount() {
    if (window.addEventListener) {
      window.addEventListener('message', this.onMessage, false);
    } else {
      window.attachEvent('onmessage', this.onMessage);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.onMessage);
  }

  onMessage = (event) => {
    const { data } = event;
    if (data.type !== 'login') {
      console.log('Unknown postMessage command:', data);
      return;
    }
    this.onLogin(data.payload, true);
  };

  onLogin = async (authData, isFakeLogin = false) => {
    try {
      this.setState({ fakeLoading: true });
      const displayName = await getUserDisplayName(authData);

      localStorage.setItem('mx_homeserver_url', authData.hsUrl);
      localStorage.setItem('mx_user_id', authData.userId);
      localStorage.setItem('mx_access_token', authData.accessToken);
      localStorage.setItem('nv_display_name', displayName);

      this.setState({
        authData: {
          ...authData,
          displayName,
        },
        isLoggedIn: true,
        fakeLoading: false,
        isFakeLogin,
      });
    } catch (e) {
      localStorage.clear();
      this.setState({
        authData: {},
        isLoggedIn: false,
        fakeLoading: false,
      });
    }
  }

  handleLogout = async () => {
    const {
      authData,
      isFakeLogin,
    } = this.state;

    if (!isFakeLogin) {
      await logout(authData);
    }

    localStorage.clear();
    this.setState({
      authData: {},
      isLoggedIn: false,
    });
  }

  render() {
    const { isLoggedIn, fakeLoading } = this.state;

    return (
      <AuthContext.Provider value={this.state}>
        {isLoggedIn ? (
          <>
            <AppHeader />
            <TagList />
          </>
        ) : (
          <LoginForm fakeLoading={fakeLoading} />
        )}

      </AuthContext.Provider>
    );
  }
}

export default App;
