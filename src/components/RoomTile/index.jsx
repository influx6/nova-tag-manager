import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import './RoomTile.scss';
import AuthContext from '../../context/AuthContext';

// https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
const primaryButton = 0;


const keyCodes = {
  enter: 13,
  escape: 27,
  arrowDown: 40,
  arrowUp: 38,
  tab: 9,
};

class Room extends PureComponent {
  onKeyDown = (
    event,
    provided,
    snapshot,
  ) => {
    if (event.defaultPrevented) {
      return;
    }

    if (snapshot.isDragging) {
      return;
    }

    if (event.keyCode !== keyCodes.enter) {
      return;
    }

    event.preventDefault();

    this.performAction(event);
  };

  onClick = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    if (event.button !== primaryButton) {
      return;
    }

    event.preventDefault();

    this.performAction(event);
  };

  onTouchEnd = (event) => {
    if (event.defaultPrevented) {
      return;
    }
    event.preventDefault();
    this.props.toggleSelectionInGroup(this.props.room.roomId);
  };

  wasToggleInSelectionGroupKeyUsed = (event) => {
    const isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
    return isMac ? event.metaKey : event.ctrlKey;
  };

  wasMultiSelectKeyUsed = (event) => event.shiftKey;

  performAction = (event) => {
    const {
      room,
      toggleSelection,
      toggleSelectionInGroup,
      multiSelectTo,
    } = this.props;

    if (this.wasToggleInSelectionGroupKeyUsed(event)) {
      toggleSelectionInGroup(room.roomId);
      return;
    }

    if (this.wasMultiSelectKeyUsed(event)) {
      multiSelectTo(room.roomId);
      return;
    }

    toggleSelection(room.roomId);
  };

  renderUserAvatar() {
    const { room } = this.props;
    const { authData: { hsUrl } } = this.context;

    if (!room.avatarMxc || !hsUrl) return <div className="room__avatar--fake" />;

    const avatarPath = room.avatarMxc.substring('mxc://'.length);
    return (
      <img
        className="room__avatar"
        alt={room.displayName}
        src={`${hsUrl}/_matrix/media/r0/thumbnail/${avatarPath}?width=96&height=96&method=crop`}
      />
    );
  }

  render() {
    const {
      room, index, isSelected, selectionCount, isGhosting,
    } = this.props;

    const classNames = [
      'room',
      isSelected ? 'room--selected' : '',
      isGhosting ? 'room--ghosting' : '',
    ];

    return (
      <Draggable draggableId={room.roomId} index={index}>
        {(provided, snapshot) => {
          const shouldShowSelection = snapshot.isDragging && selectionCount > 1;

          if (snapshot.isDragging) classNames.push('room--dragging');

          return (
            <li
              className={classNames.join(' ')}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              onClick={this.onClick}
              onTouchEnd={this.onTouchEnd}
              onKeyDown={(event) => this.onKeyDown(event, provided, snapshot)}
            >
              <div className="room__content">
                {this.renderUserAvatar()}
                <p className="room__name">{room.displayName}</p>
                {/* <span className="room__bridge-name">Slack</span> */}
              </div>
              {shouldShowSelection ? (
                <small className="room__selection-count">{selectionCount}</small>
              ) : null}
            </li>
          );
        }}
      </Draggable>
    );
  }
}

Room.contextType = AuthContext;

Room.propTypes = {
  room: PropTypes.shape().isRequired,
  index: PropTypes.number.isRequired,
  isSelected: PropTypes.bool.isRequired,
  isGhosting: PropTypes.bool.isRequired,
  selectionCount: PropTypes.number.isRequired,
  toggleSelection: PropTypes.func.isRequired,
  toggleSelectionInGroup: PropTypes.func.isRequired,
  multiSelectTo: PropTypes.func.isRequired,
};

export default Room;
