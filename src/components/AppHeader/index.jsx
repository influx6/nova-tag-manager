import React, { useContext } from 'react';
// import PropTypes from 'prop-types';
import './AppHeader.scss';
import AuthContext from '../../context/AuthContext';
import Button from '../commons/Button';

const AppHeader = () => {
  const { onLogout, authData: { displayName } } = useContext(AuthContext);

  return (
    <div className="header">
      <div>
        Logged in as {displayName}
      </div>
      <Button onClick={onLogout}>Logout</Button>
    </div>
  );
};

AppHeader.defaultProps = {

};

AppHeader.propTypes = {

};

export default AppHeader;
