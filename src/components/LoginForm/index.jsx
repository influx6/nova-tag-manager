import React, { useContext, useReducer, useCallback } from 'react';
import PropTypes from 'prop-types';
import './LoginForm.scss';
import AuthContext from '../../context/AuthContext';
import Button from '../commons/Button';
import { login } from '../../utils/auth';

const initialState = {
  data: {
    username: '',
    password: '',
  },
  loading: false,
  error: '',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'form-value':
      const { key, value } = action.payload;
      return {
        ...state,
        data: { ...state.data, [key]: value },
      };
    case 'loading':
      return { ...state, loading: action.payload };
    case 'set-error':
      return { ...state, error: action.payload };
    case 'reset':
      return initialState;
    default:
      throw new Error();
  }
};

const LoginForm = (props) => {
  const { onLogin } = useContext(AuthContext);
  const [state, dispatch] = useReducer(reducer, initialState);

  const { data, loading, error } = state;

  const handleSubmit = useCallback(
    async (event) => {
      event.preventDefault();

      const { username, password } = state.data;
      if (!username || !password) return;

      dispatch({ type: 'loading', payload: true });
      try {
        const hsUrl = `https://${username}.nova.chat`;
        const { user_id: userId, access_token: accessToken } = await login({ hsUrl, username, password });

        onLogin({ hsUrl, userId, accessToken }, false);
      } catch (e) {
        dispatch({ type: 'loading', payload: false });
        if (e.error && e.error.error) {
          dispatch({ type: 'set-error', payload: e.error.error });
        } else {
          dispatch({ type: 'set-error', payload: 'Error logging in' });
        }
      }
    }, [state.data, onLogin],
  );

  return (

    <div className="login-form">
      <h4 className="login-form__title">Welcome to Tag Manager !</h4>
      <p className="login-form__text">Sign in with your NovaChat account</p>
      <form className="login-form__form" onSubmit={handleSubmit}>

        <div className="login-form__row">
          <label
            htmlFor="username"
            className="login-form__label"
          >
            Username
          </label>
          <input
            className="login-form__field"
            tabIndex="0"
            id="username"
            name="username"
            type="text"
            value={data.username}
            onChange={(event) => dispatch({
              type: 'form-value',
              payload: { key: 'username', value: event.target.value },
            })}
          />
        </div>
        <div className="login-form__row">
          <label
            htmlFor="password"
            className="login-form__label"
          >
            Password
          </label>
          <input
            className="login-form__field"
            tabIndex="0"
            id="password"
            name="password"
            type="password"
            value={data.password}
            onChange={(event) => dispatch({
              type: 'form-value',
              payload: { key: 'password', value: event.target.value },
            })}
          />
        </div>
        <div>{error}</div>
        <div className="login-form__row">
          <Button tabIndex="0" type="submit" loading={loading || props.fakeLoading}>Log In</Button>
        </div>
      </form>
    </div>
  );
};

LoginForm.defaultProps = {
  size: 'small',
};

LoginForm.propTypes = {
  size: PropTypes.string,
};

export default LoginForm;
